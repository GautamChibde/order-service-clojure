(defproject order_service "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [compojure "1.5.2"]
                 [hiccup "1.0.5"]
                 [ring-server "0.4.0"]
                 [environ "1.0.0"]
                 ;; Database
                 [org.postgresql/postgresql
                  "9.3-1102-jdbc41"]
                 [yesql "0.5.1"]
                 [com.zaxxer/HikariCP "2.4.1"]
                 [clojure.jdbc/clojure.jdbc-c3p0 "0.3.1"]
                 ;; Web
                 [ring/ring-defaults "0.1.5"]
                 [ring/ring-json "0.4.0"]
                 [ring-cors "0.1.7"]
                 [http-kit "2.1.19"]
                 [liberator "0.13"]
                 ;; Auth
                 [clauth "1.0.0-rc17"]
                 ;; Comm
                 [clojurewerkz/mailer "1.2.0"]
                 [cheshire "5.2.0"]
                 ;; Util
                 [com.taoensso/timbre "4.2.1"]
                 [com.fzakaria/slf4j-timbre "0.3.0"]]
  :min-lein-version "2.0.0"
  :plugins [[lein-ring "0.8.12"]
            [lein-environ "1.0.0"]]
  :uberjar-name "order_service-standalone.jar"
  :ring {:handler order_service.handler/app
         :init order_service.handler/init
         :destroy order_service.handler/destroy}
  :profiles
  {:uberjar {:aot :all}
   :production
   {:ring
    {:open-browser? false, :stacktraces? false, :auto-reload? false}}
   :dev
   {:dependencies [[ring-mock "0.1.5"] [ring/ring-devel "1.5.1"]]}})
