-- name: q-create-order_schema!
DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'order_status') THEN
        CREATE TYPE order_status AS enum('new','placed','complete');
    END IF;
END$$;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS ordr(
	id SERIAL PRIMARY KEY,
	uuid TEXT unique NOT NULL,
	customer_id INTEGER REFERENCES customer NOT NULL,
	status order_status DEFAULT 'new',
	created_stamp timestamp(0) DEFAULT now(),
  	updated_stamp timestamp(0) DEFAULT now()
);

CREATE TABLE IF NOT EXISTS item(
	id SERIAL PRIMARY KEY,
	product_id INTEGER REFERENCES product,
	order_id INTEGER REFERENCES ordr,
	quantity INTEGER,
	created_stamp timestamp(0) DEFAULT now(),
  	updated_stamp timestamp(0) DEFAULT now()
);

CREATE OR REPLACE VIEW order_doc AS
	SELECT 
		od.id,
		od.uuid,
		od.customer_id,
		od.status,
		cast(od_it.items AS jsonb)
  	FROM ordr AS od
   		LEFT OUTER JOIN
   			(SELECT od.id,
   				json_agg((SELECT row_to_json(t) 
   					FROM (SELECT 
   							it.id,
   							it.quantity,
                            (SELECT row_to_json(p)
                                FROM (SELECT 
                                  		pd.id,
                                  		pd.name,
                                  		pd.detail,
                                  		pd.category)p) AS "product")t)) AS items 
           	FROM ordr AS od
                LEFT OUTER JOIN item AS it
                  	ON od.id = it.order_id
                LEFT OUTER JOIN (SELECT
					    p.id,p.name,
					    p.created_stamp,p.updated_stamp,
					    cast(row_to_json(pc) AS jsonb) AS category,
					    cast(row_to_json(pd) AS jsonb) AS detail
					FROM product AS p
					    LEFT OUTER JOIN (SELECT 
				           id,name 
				           FROM product_category AS pc) pc 
				        ON p.product_category_id = pc.id
				        LEFT OUTER JOIN (SELECT 
				            id,price,rating,image,description 
				            FROM product_detail AS pd) pd
				        ON p.product_detail_id = pd.id) pd
                  	ON it.product_id = pd.id
           		GROUP BY od.id) od_it
         	ON od.id = od_it.id;

CREATE OR REPLACE FUNCTION order_for_user(user_id INTEGER)
    returns setof order_doc
    LANGUAGE 'plpgsql'
AS $$
    declare
        l_detail_id INTEGER;
    BEGIN
    	return query (SELECT * FROM order_doc WHERE customer_id = user_id);
    END;
$$;

CREATE OR REPLACE FUNCTION insert_order(user_id INTEGER,in_ordr json)
    returns setof order_doc
    LANGUAGE 'plpgsql'
AS $$
    declare
        l_ordr_id INTEGER;
        l_itm json;
    BEGIN
    	INSERT INTO ordr(uuid,customer_id)
      		VALUES(uuid_generate_v4(),user_id)
      	returning id INTO l_ordr_id;
      	FOR l_itm IN
        	SELECT * FROM json_array_elements(in_ordr->'items')
      	loop
        	perform insert_item(l_ordr_id,l_itm);
      	END loop;
      	return query (SELECT * FROM order_doc WHERE id = l_ordr_id);
    END;
$$;

CREATE OR REPLACE FUNCTION insert_item(in_ordr_id integer,in_item json)
    returns integer
    LANGUAGE 'plpgsql'
AS $$
    declare
        l_item_id integer;
    BEGIN
      INSERT INTO item(order_id,product_id,quantity)
      VALUES(in_ordr_id,
             cast(in_item#>>'{product,id}' AS integer),
             cast(in_item->>'quantity' AS integer))
      returning id INTO l_item_id;
      return l_item_id;
    END;
$$;