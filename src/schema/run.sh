#!/usr/bin/env bash
#
dropdb order_service_prod
#
createdb order_service_prod

cd ../../../../../sq/src/schema/query
#
scripts=(product.sql 
         order.sql)
for f in ${scripts[@]};
do
    psql sq ${USER} -f "$f"
done
#
exit 0
