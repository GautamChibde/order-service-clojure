(ns order_service.handler
  (:require [compojure.core :refer [defroutes routes]]
            [ring.middleware.resource :refer [wrap-resource]]
            [ring.middleware.file-info :refer [wrap-file-info]]
            [hiccup.middleware :refer [wrap-base-url]]
            [compojure.handler :as handler]
            [order_service.db :as db]
            [yesql.core :refer [defqueries]]
            [order_service.util :as u]
            [compojure.route :as route]
            [order_service.routes :as r]))

(defqueries
  "order_service/schema/product.sql"
  {:connection db/pooled-db-spec})

(defn create-product_schema []
  (q-create-product_schema!))

(defqueries
  "order_service/schema/user.sql"
  {:connection db/pooled-db-spec})

(defn create-user_schema []
  (q-create-user_schema!))

(defqueries
  "order_service/schema/order.sql"
  {:connection db/pooled-db-spec})

(defn create-order_schema []
  (q-create-order_schema!))

(defn init []
  (do 
    (println "order_service is starting")
    (create-product_schema)
    (create-user_schema)
    (create-order_schema)))

(defn destroy []
  (println "order_service is shutting down"))

(defroutes app-routes
  (route/resources "/")
  (route/not-found "Not Found"))

(def app
  (-> (routes r/api-routes r/common-routes app-routes)
      (handler/site)
      (wrap-base-url)))
