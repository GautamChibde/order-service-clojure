(ns order_service.domain.controller.order
	(:require 
		[compojure.core :refer :all]
    [taoensso.timbre :as timbre]
    [cheshire.core :refer [generate-string]]
    [order_service.db :as db]
    [yesql.core :refer [defqueries]]
    [order_service.util :as u]
		[liberator.core :refer [defresource]]
		[liberator.representation :refer [ring-response]]))

(def json-col [:items])

(defqueries
  "order_service/domain/query/order.sql"
  {:connection db/pooled-db-spec})

(defn get-order [id]
	(q-order {:id (u/parse-int id)} {:row-fn (u/to-map json-col)}))

(defn insert-order [id order-to-save]
	(let [order
        order-to-save
        saved-order
        (q-save
         {:order order :id id}
         {:row-fn (u/to-map json-col)})]
    (timbre/info saved-order)
    (first saved-order)))

(defresource order
	:available-media-types ["application/json"]
  :allowed-methods [:get :post :delete]
  :handle-ok (fn [ctx]
  	(let [params (get-in ctx [:request :params])]
			(if (contains? params :user_id)
			  (get-order (params :user_id))
			  (ring-response
          {:status 403 
           :body (u/error-params "user_id")}))))
  :post! (fn [ctx]
  	{::data
  		(let [params (get-in ctx [:request :params])]
  			(if (contains? params :user_id)
  				(insert-order (params :user_id) (slurp (get-in ctx [:request :body])))
  				(ring-response
          	{:status 403 
           	 :body (u/error-params "user_id")})))})
  :handle-created ::data)