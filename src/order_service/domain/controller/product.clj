(ns order_service.domain.controller.product
  (:require [compojure.core :refer :all]
            [taoensso.timbre :as timbre]
            [cheshire.core :refer [generate-string]]
            [order_service.db :as db]
            [yesql.core :refer [defqueries]]
            [order_service.util :as u]
   			[liberator.core :refer [defresource]]
        [liberator.representation :refer [ring-response]]))

(def json-col [:detail :category])

(defqueries
  "order_service/domain/query/product.sql"
  {:connection db/pooled-db-spec})

(defn get-prod []
  (q-all {} {:row-fn (u/to-map json-col)}))

(defn get-prod-cat [id]
	(q-all-cat {:category_id (u/parse-int id)} {:row-fn (u/to-map json-col)}))

(defn get-cat []
  (q-cat {} {:row-fn (u/to-map [])}))

(defn save-product [products-to-save]
	(let [product
        products-to-save
        saved-product
        (q-save
         {:product product}
         {:row-fn (u/to-map json-col)})]
    (timbre/info saved-product)
    (first saved-product)))

(defn save-product-category [product-category-to-save]
  (let [product_category
        product-category-to-save
        saved-product-category
        (q-save-cat
         {:product_category product_category}
         {:row-fn (u/to-map [])})]
    (timbre/info saved-product-category)
    (first saved-product-category)))

(defn update-product [id products-to-update]
  (let [product
        products-to-update
        updated-product
        (q-update
         {:product product :id id}
         {:row-fn (u/to-map json-col)})]
    (timbre/info updated-product)
    (first updated-product)))

(defn delete-product [id]
  (q-delete-product {:id (u/parse-int id)} {}))
(defn delete-product-category [id]
  (q-delete-product-category {:id (u/parse-int id)} {}))

(defresource product
	:available-media-types ["application/json"]
  :allowed-methods [:get :post :put :delete]

	:handle-ok (fn [ctx]
  	(let [params (get-in ctx [:request :params])]
			(if (contains? params :category)
			  (get-prod-cat (params :category))
			  (get-prod))))

  :post! (fn [ctx]
    {::data
  		(save-product (slurp (get-in ctx [:request :body])))})

  :put! (fn [ctx]
    {::data
      (let [params (get-in ctx [:request :params])]
          (if (contains? params :id)
            (update-product (params :id) (slurp (get-in ctx [:request :body])))
            (ring-response
              {:status 403 
               :body (u/error-params "category")})))})

  :delete! (fn [ctx]
    {::data
      (let [params (get-in ctx [:request :params])]
        (if (contains? params :id)
          (delete-product (params :id))
          (ring-response
            {:status 403 
             :body (u/error-params "id")})))})
   
  :handle-created ::data)

(defresource home
  :available-media-types ["application/json"]
  :allowed-methods [:get :post :delete]
  :handle-ok (fn [ctx]
                (assoc {} :name "scsc" :type "dffsfsf")))

(defresource product-category
	:available-media-types ["application/json"]
  :allowed-methods [:get :post :delete]
  :handle-ok (get-cat)
  :post! (fn [ctx]
    {::data
      (save-product-category (slurp (get-in ctx [:request :body])))})
  :delete! (fn [ctx]
    {::data
      (let [params (get-in ctx [:request :params])]
        (if (contains? params :id)
          (delete-product-category (params :id))
          (ring-response
            {:status 403 
             :body (u/error-params "id")}))) })
  :handle-created ::data)