(ns order_service.domain.controller.user
	(:require 
		[compojure.core :refer :all]
    [taoensso.timbre :as timbre]
    [cheshire.core :refer [generate-string]]
    [order_service.db :as db]
    [yesql.core :refer [defqueries]]
    [order_service.util :as u]
		[liberator.core :refer [defresource]]))

(defresource test-r
	:available-media-types ["application/json"]
  :allowed-methods [:get :post :put :delete]
  :handle-ok (fn [ctx]
                (assoc {} 
                  :name "scsc" 
                  :type "dffsfsf")))
