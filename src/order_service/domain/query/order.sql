-- name:q-order
select id,uuid,status::text,items::text from order_for_user(cast(:id as integer));

-- name:q-save
select id,uuid,status::text,items::text from insert_order(cast(:id as integer),cast(:order as json));