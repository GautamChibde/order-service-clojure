-- name: q-all
SELECT id,name,detail::text,category::text FROM product_doc;

-- name: q-all-cat
SELECT id,name,detail::text,category::text FROM product_doc WHERE cast (category->>'id' AS integer) = :category_id;

-- name: q-cat
SELECT id,name FROM product_category WHERE state = 'active';

-- name: q-save-cat
SELECT id,name FROM insert_product_category(cast(:product_category AS json));

-- name: q-delete-product-category
update product_category SET state = 'archived',updated_stamp = now() WHERE id = :id RETURNING id;

-- name: q-save
SELECT id,name,detail::text,category::text FROM insert_product(cast(:product AS json));

-- name: q-delete-product
update product SET state = 'archived',updated_stamp = now() WHERE id = :id RETURNING id;

-- name: q-update
SELECT id,name,detail::text,category::text FROM update_product(cast(:id AS integer),cast(:product AS json));