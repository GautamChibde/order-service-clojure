(ns order_service.db
  (:require
    [environ.core :refer [env]]
    [clojure.java.jdbc :as j]
    [cheshire.core :refer [generate-string parse-string]])
  (:import com.zaxxer.hikari.HikariDataSource))

(def pooled-db-spec {:classname
              "org.postgresql.Driver"
              :subprotocol
              "postgresql"
              :subname "//ec2-54-235-153-124.compute-1.amazonaws.com:5432/dfg2orr19alb8l"
              :init-pool-size 4
              :max-pool-size  20
              :user "rfwucfdquxgivx"
              :password "d30b935fb379b5d3b3a0b979afdd8a67baa60aa09c220f4afba8b7b540d3b59d"
              :partitions     2})
