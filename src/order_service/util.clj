(ns order_service.util
  (:require [taoensso.timbre :as timbre]
            [cheshire.core :refer [parse-string]]))

(timbre/refer-timbre)
(timbre/set-level! :info)

(def not-nil? (complement nil?))

(defn request-body [ctx]
   (get-in ctx [:request :params :root]))

(defn error-params [text] 
  (str "{\"message\" :\"" text " not found in params\"}"))

(defn log-request [ctx]
  (timbre/debug "Request : " + (request-body ctx)))

(def valid-chars
  (map char (concat (range 48 58)                           ; 0-9
                    (range 65 91)                           ; A-Z
                    (range 97 123))))                       ; a-z

(defn random-string [length]
  (apply str (take length (repeatedly #(rand-nth valid-chars)))))

(defn random-number [length]
  (apply str (take length (repeatedly #(rand-nth (range 0 9))))))

(defn update-each
  "Updates each keyword listed in ks on associative structure
  m using fn."
  [ks f m]
  (reduce #(update-in %1 [%2] f) m ks))

(defn dissoc-with-pred [f m]
  (into {} (filter (fn [[k v]] (f k v)) m)))

;; (defn row-to-map [v]
;;   (let [ps (parse-string v true)]
;;     (if (or (empty? ps)
;;             (and (map? ps) (contains? ps :id) (nil? (:id ps)))
;;             (and (seq? ps) (= 1 (count ps)) (nil? ((first ps) :id))))
;;       nil
;;       ps)))

(defn row-to-map
  [v]
  (parse-string v true))

(defn to-map
  [ks]
  (partial update-each ks row-to-map))

(defmacro try-catch [[lvl-fn fn] body]
  `(try
     ~body
     (catch Exception e#
       (~lvl-fn e# (str "Exception thrown trying to " (:name (meta #'~fn)))))))







;;;;;;;;;;;; NEST JOINED START ;;;;;;;;;;;;;;;;;;
(defn- group-result-by-key [rs pk]
  (vals (group-by #(pk %) rs)))

(defn- joined-vec-to-map [jks nk]
  (fn
    ([] {})
    ([l r]
     (if (nk l)
       (assoc l nk
              (concat (nk l) [(select-keys r jks)]))
       (assoc l nk
              [(select-keys l jks)
               (select-keys r jks)])))))

(defn- dissoc-joined [rec]
  (fn [jks jpk nk]
    (if (or (nil? (jpk rec))(nk rec))
      (apply dissoc rec jks)
      (apply dissoc
             (assoc rec nk (select-keys rec jks))
             jks))))

(defn parse-int [s]
   (Integer. (re-find  #"\d+" s )))

(defn nest-joined [rs jks nk pk jpk]
  (map
   #((dissoc-joined %) jks jpk nk)
   (map
    #(reduce (joined-vec-to-map jks nk) %)
    (group-result-by-key rs pk))))

;;;;;;;;;;;; NEST JOINED END ;;;;;;;;;;;;;;;;;;
