-- name: q-create-product_schema!
DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'state_type') THEN
        CREATE TYPE state_type AS enum('new','active','archived');
    END IF;
END$$;

CREATE TABLE IF NOT EXISTS product_category(
	id SERIAL PRIMARY KEY,
	name TEXT NOT NULL,
	state state_type DEFAULT 'active',
	created_stamp timestamp(0) DEFAULT now(),
  	updated_stamp timestamp(0) DEFAULT now()
);

CREATE TABLE IF NOT EXISTS product_detail(
	id SERIAL PRIMARY KEY,
	price INTEGER,
	image TEXT,
	rating INTEGER,
  description TEXT,
	state state_type DEFAULT 'active',
	created_stamp timestamp(0) DEFAULT now(),
  	updated_stamp timestamp(0) DEFAULT now()
);

CREATE TABLE IF NOT EXISTS product(
	id SERIAL PRIMARY KEY,
	name TEXT NOT NULL,
	product_category_id INTEGER REFERENCES product_category,
	product_detail_id INTEGER REFERENCES product_detail,
	state state_type DEFAULT 'active',
	created_stamp timestamp(0) DEFAULT now(),
  	updated_stamp timestamp(0) DEFAULT now()
);

CREATE OR REPLACE VIEW product_doc AS
    SELECT
      p.id,p.name,
      p.created_stamp,p.updated_stamp,
      cast(row_to_json(pc) AS jsonb) AS category,
      cast(row_to_json(pd) AS jsonb) AS detail
    FROM product AS p
        LEFT OUTER JOIN (SELECT 
            id,name 
            FROM product_category AS pc) pc 
          ON p.product_category_id = pc.id
        LEFT OUTER JOIN (SELECT 
            id,price,rating,image,description 
            FROM product_detail AS pd) pd
          ON p.product_detail_id = pd.id
        WHERE p.state = 'active';

CREATE OR REPLACE VIEW product_category_doc AS
    SELECT
      pc.id,pc.name
    FROM product_category AS pc
    WHERE pc.state = 'active';

CREATE OR REPLACE FUNCTION insert_product_detail(in_detail json)
    returns INTEGER
    LANGUAGE 'plpgsql'
AS $$
    declare
        l_detail_id INTEGER;
    BEGIN
    	INSERT INTO product_detail(price, image, rating, description)
	        VALUES(cast(in_detail->>'price' AS INTEGER),
                 in_detail->>'image',
	            	 cast(in_detail->>'rating' AS INTEGER),
                 in_detail->>'description')
	    returning id INTO l_detail_id;
	    return l_detail_id;
    END;
$$;

CREATE OR REPLACE FUNCTION update_product_detail(in_id INTEGER, in_detail json)
    returns void
    LANGUAGE 'plpgsql'
AS $$
    BEGIN
    	UPDATE product_detail SET 
    		price = cast(in_detail->>'price' AS INTEGER),
    		image = in_detail->>'image',
    		rating = cast(in_detail->>'rating' AS INTEGER),
    		updated_stamp = now()
    	WHERE id = in_id;
    END;
$$;

CREATE OR REPLACE FUNCTION insert_product(in_product json)
    returns setof product_doc
    LANGUAGE 'plpgsql'
AS $$
    declare
        l_product_detail_id INTEGER;
        l_product_id INTEGER;
    BEGIN
        SELECT insert_product_detail(in_product->'detail')
            INTO l_product_detail_id;
	    INSERT INTO product(name, product_detail_id, product_category_id)
	        VALUES(in_product->>'name',l_product_detail_id,
	            	cast(in_product#>>'{category,id}' AS INTEGER))
	    returning id INTO l_product_id;
	    return query (SELECT * FROM product_doc WHERE id = l_product_id);
    END;
$$;

CREATE OR REPLACE FUNCTION update_product(in_id INTEGER,in_product json)
    returns setof product_doc
    LANGUAGE 'plpgsql'
AS $$
    declare
        l_product_detail_id INTEGER;
    BEGIN
    	SELECT product_detail_id INTO l_product_detail_id FROM product WHERE id = in_id; 
    	perform update_product_detail(l_product_detail_id,in_product->'detail');
       	UPDATE product SET 
       		name = in_product->>'name',
       		product_category_id = cast(in_product#>>'{category,id}' AS INTEGER),
       		updated_stamp = now()
       	WHERE id = in_id;
	    return query (SELECT * FROM product_doc WHERE id = in_id);
    END;
$$;

CREATE OR REPLACE FUNCTION insert_product_category(in_product_category json)
    returns setof product_category_doc
    LANGUAGE 'plpgsql'
AS $$
    declare
        l_id INTEGER;
    BEGIN
      INSERT INTO product_category(name) VALUES(in_product_category->>'name') returning id INTO l_id;
      return query (SELECT * FROM product_category_doc WHERE id = l_id);
    END;
$$;