-- name: q-create-user_schema!
CREATE TABLE IF NOT EXISTS customer(
	id SERIAL primary key,
	name TEXT NOT NULL,
	email TEXT NOT NULL,
	pswd TEXT NOT NULL,
	state state_type default 'active',
	created_stamp timestamp(0) default now(),
  	updated_stamp timestamp(0) default now()
);

insert into customer (name,email,pswd) values ('test','test@test.co','ssd');