(ns order_service.routes
	(:require 
		[taoensso.timbre :as timbre]
		(compojure
    		[core :refer :all])
		(order_service.domain.controller
			[product :refer [product product-category home]]
			[order :refer [order]]
			[user :refer [test-r]])))

(defroutes common-routes
  ;(POST "/api/signup" [] signup)
  ;(POST "/api/token" [] login)
  ;(GET "/api/connect" [] socket-handler)
)

(defroutes api-routes
	(ANY "/" request (home request))
	(ANY "/product" request (product request))
	(ANY "/test" request (test-r request))
	(ANY "/product_category" request (product-category request))
	(ANY "/signup" request (product-category request))
	(ANY "/login" request (product-category request))
	(ANY "/order" request (order request)))