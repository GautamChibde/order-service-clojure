import urllib2
from bs4 import BeautifulSoup as soup 

hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
       'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
       'Accept-Encoding': 'none',
       'Accept-Language': 'en-US,en;q=0.8',
       'Connection': 'keep-alive'}
#url = 'https://www.foodpanda.in/restaurant/v6qc/sannidhi-restaurant-hinjewadi'
#url = 'https://www.swiggy.com/bangalore/baba-da-dhaba-cbd-central-bangalore'
url = 'https://order.faasos.io/collections'
print "downloading"
req = urllib2.Request(url, headers=hdr)
page = urllib2.urlopen(req)
soup = soup(page.read(), 'html.parser')
page.close()
print "downloading complete"

#with open()
#for items in soup.findAll('ul',{"class":"menu-item__variations"}):
#	print '{name = ' + str(items.article['data-clicked_product_name'].strip()) +', cat = ' + str(items.article['data-clicked_product_category_name'].strip()) + "}"

