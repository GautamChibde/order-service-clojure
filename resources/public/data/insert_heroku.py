
#Faasos data
"""
with open('faasos.json') as data_file:
    data = json.load(data_file)
    for d in data['collections']:
    	cur.execute("SELECT id FROM insert_product_category(cast('{0}' AS json));".format(json.dumps({'name':d['collection_name']})))
    	pc_id = int(cur.fetchone()[0])
    	for menu in d['products']:
    		detail = {'price' : menu['price'],'rating' : int(menu['rating']),'image':menu['product_image'],'description':menu['description']}
    		product = {'name':menu['product_name'],'category': {'id' : pc_id},'detail':detail}
    		cur.execute("SELECT insert_product(cast('{0}' AS json))".format(json.dumps(product)))
"""

import json
import urllib2
import time

req = urllib2.Request('http://192.168.1.14:3000/product_category')
req.add_header('Content-Type', 'application/json')

with open('faasos.json') as data_file:
    data = json.load(data_file)
    for d in data['collections']:
    	if "&" not in d['collection_name']: 
	    	body = json.dumps({'name':d['collection_name']})
	    	urllib2.urlopen(req, body)
		time.sleep(60)
