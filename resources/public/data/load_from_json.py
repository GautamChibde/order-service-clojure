import json
from pprint import pprint
output_file = open('data.csv','w')
output_file.write('name,category,description,price,itemScore,image\n')

with open('data.json') as data_file:
    data = json.load(data_file)
    for d in data['categories']:
    	for menu in d['menu']:
    		output_file.write(menu['name'].replace(',','')+','+d['name'].replace(',','')+','+menu['description'].replace(',','')+','+ str(menu['price']/100) +',' +str(menu['itemScore']) + ','+menu['cloudinaryImageId']+ '\n')

output_file.close()